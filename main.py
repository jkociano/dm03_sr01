#!/usr/bin/python
# -*- coding: UTF-8 -*-
# jeu de la vie

from random import *
import os
import copy
from time import sleep
from tkinter import *

grille = {}
cellSize = 20
cellColour = "#fed766"
deadColour = "#272727"
tmps_gen = 1000
taille = 30
drapeauAnim = 0
drapeau = 0
vivantes = 5


def initialiserDamier(vie, N):
    seed(a=None, version=2)
    for i in range(N):
        for j in range(N):
            vie[(i, j)] = 0


def remplirDamier(vie, N):
    seed(a=None, version=2)
    for i in range(N):
        for j in range(N):
            vie[(i, j)] = randrange(0, N)%vivantes

#affiche le jeu de la vie dans la console
def afficher(vie, N):
    for i in range(N):
        for j in range(N):
            if (vie[(i, j)]):
                print("*", end=" ")
            else:
                print(" ", end=" ")
        print()
    print()



#########################################################################
# nom : effectuer
# valeurs entree : recupere le dico sans le passer en parametere car il
#                  est une variable global.
# valeurs sortie : modifie le dico.
# fonction : applique l'algorithme pour allez a T+1.
#########################################################################
def effectuer(vie, MAX):  # on fait une copie qu'on modifie
    tmp = {}
    tmp = copy.copy(vie)
    for i in range(MAX):
        for j in range(MAX):
            vivant = 0
            if (tmp[((j + MAX - 1) % MAX, i % MAX)]):
                vivant += 1
            if (tmp[((j + MAX - 1) % MAX, (i + MAX - 1) % MAX)]):
                vivant += 1
            if (tmp[((j + MAX - 1) % MAX, (i + 1) % MAX)]):
                vivant += 1
            if (tmp[((j + 1) % MAX, i % MAX)]):
                vivant += 1
            if (tmp[((j + 1) % MAX, (i + MAX - 1) % MAX)]):
                vivant += 1
            if (tmp[(j + 1) % MAX, (i + 1) % MAX]):
                vivant += 1
            if (tmp[j % MAX, (i + MAX - 1) % MAX]):
                vivant += 1
            if (tmp[j % MAX, (i + 1) % MAX]):
                vivant += 1

            if (tmp[j, i]):
                if (vivant >= 2 and vivant <= 3):
                    vie[(j, i)] = 1
                if (vivant >= 4):
                    vie[(j, i)] = 0
                if (vivant <= 1):
                    vie[(j, i)] = 0
            else:
                if (vivant == 3):
                    vie[(j, i)] = 1
    return vie


#########################################################################
# nom : lancer_jeu
# valeurs entree : nombre de case du tableau.
# valeurs sortie : aucune
# fonction : initialiser le jeu et l'organiser.
#########################################################################

#affiche jeu de la vie dansla fênetre faite avec tkinter
def dessiner():
    x = 0
    y = 0
    longueur = int(size.get())
    for i in range(longueur):
        x = 0
        for j in range(longueur):
            if grille[(i, j)] == 1:
                can.create_rectangle(x, y, x + cellSize, y + cellSize, fill=cellColour, outline="black")
            else:
                can.create_rectangle(x, y, x + cellSize, y + cellSize, fill=deadColour, outline="black")
            # on se déplace d'une cellule
            x = x + cellSize
        # on se déplace d'une ligne
        y = y + cellSize


def animation():
    global drapeauAnim
    if drapeauAnim:
        global grille
        grille = effectuer(grille, taille)
        can.delete(ALL)
        # on dessine la nouvelle grille
        dessiner()
        # pour générer la génération suivante :
        window.after(tmps_gen, animation)

"""
def lancer_jeu(MAX=40):
    vie = {}
    init(vie, MAX)
    # while (1):
    while (drapeau == 1):
        afficher(vie, MAX)
        sleep(1)
        os.system("cls")  # in windows : cls instead of clear in linux
        vie = effectuer(vie, MAX)
"""


def pressedLancer():
    global drapeau
    global drapeauAnim
    global tmps_gen
    drapeauAnim = 1
    tmps_gen = int(1000/int(vitesse.get()))

    if drapeau == 0:
        animation()

    drapeau = 1



def pressedArreter():
    global drapeau
    global drapeauAnim
    drapeau = 0
    drapeauAnim = 0
    print("j'ai appuyé sur arrêter haha")


def pressedInitialiser():
    print("je veux initialiser le jeu de la vie")
    global vie
    global drapeauAnim
    global drapeau
    global taille
    global can
    global vivantes
    drapeauAnim = 0
    drapeau = 0

    # on modifie la taille du canvas
    taille = int(size.get())
    can["width"] = taille * cellSize
    can["height"] = taille * cellSize


    # on modifie le % de vie à l'initialisation :
    pourcentage = int(pourcentageVie.get())
    vivantes = int(100/pourcentage)

    # on initialise la nouvelle grille
    remplirDamier(grille, taille)

    # on dessine
    dessiner()

    global tmps_gen
    tmps_gen = int(1000 / int(vitesse.get()))


#########################################################################
# nom : main
# valeurs entree : aucune.
# valeurs sortie : aucune
# fonction : initialiser le jeu et l'organiser.
#########################################################################
# def main():

window = Tk()
window.title("Jeu de la vie de Côme et Julie")
window.resizable(width=FALSE,height=FALSE)

quadrillage = Frame(window, bd=2)
quadrillage.pack(side=LEFT)
menu = Frame(window, bd=4)
menu.pack(side=RIGHT)

can = Canvas(quadrillage, height=taille * cellSize, width=taille * cellSize, bg="white")
can.focus_set()
can.pack()

lancer = Button(menu, text="Lancer", command=pressedLancer)
lancer.pack(side=TOP,anchor=CENTER)

arreter = Button(menu, text="Arrêter", command=pressedArreter)
arreter.pack(side=TOP,anchor=CENTER)

initialiser = Button(menu, text="Initialiser", command=pressedInitialiser)
initialiser.pack(side=TOP,anchor=CENTER)

quitter = Button(menu, text="Quitter", command=window.destroy)
quitter.pack(side=TOP,anchor=CENTER)

size = IntVar()
choix_taille = Scale(menu, variable=size, orient=HORIZONTAL, from_=10, to=60, resolution=10, label="Taille")
choix_taille.pack(side = BOTTOM, anchor = CENTER)

global vitesse
vitesse = IntVar()
choix_vitesse = Scale(menu, variable=vitesse, orient=HORIZONTAL, from_=1, to=50, resolution=1, label="Vitesse")
choix_vitesse.pack(side = BOTTOM, anchor = CENTER)

global pourcentageVie
pourcentageVie = IntVar()
choix_pourcentage = Scale(menu, variable=pourcentageVie, orient=HORIZONTAL, from_=1, to=100, resolution=1, label="% de vie")
choix_pourcentage.pack(side = BOTTOM, anchor = CENTER)

message = Label(quadrillage, text="Déroulement du jeu de la vie")
message.pack()
# lancer_jeu(20)
window.mainloop()

# if __name__ == "__main__":
#   main()

message = Label(quadrillage, text = "Julie c'est la plus belle")
message.pack()
#lancer_jeu(20)

window.mainloop()


#if __name__ == "__main__":
#   main()
