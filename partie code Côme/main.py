#!/usr/bin/python
# -*- coding: UTF-8 -*-
# jeu de la vie

from random import *
import os
import copy
from time import sleep
from tkinter import *

grille = {}
vie = 5
t_cellule = 20
c_cellule = "red"
c_case_vide = "white"
tmps_gen = 1000
taille = 30
drapeau_animation = 0
drapeau_lancer = 0

def initialiser_dam_zero(dam, N) : 
    seed(a=None, version=2)
    for i in range(N) : 
        for j in range(N) : 
            dam[(i,j)] = 0


def initialiser_dam(dam, N):
    seed(a = None, version = 2)
    for i in range(N):
        for j in range(N):
            dam[(i,j)] = randrange(0,N)%vie

def afficher(dam, N) : 
    for i in range(N):
        for j in range(N):
            if (dam[(i,j)]):
                print("*", end=" ")
            else:
                print(" ", end =" ")
        print()
    print()

#def init(vie, MAX):
#    random.seed(a=None, version=2)
#
#    for i in range(MAX):
#        for j in range(MAX):
#            vie[(i, j)] = random.randrange(0, MAX) % 2


# affiche la matrice dans la console.
#def afficher(vie, MAX):
 #   for i in range(MAX):
  #         if vie[(i, j)]:
   #             print(end="*")
   #         else:
    #            print(end=" ")
    #    print()
    #print()
##

#########################################################################
# nom : effectuer
# valeurs entree : recupere le dico sans le passer en parametere car il
#                  est une variable global.
# valeurs sortie : modifie le dico.
# fonction : applique l'algorithme pour allez a T+1.
#########################################################################
def effectuer(vie, MAX):  # on fait une copie.
    tmp = {}
    tmp = copy.copy(vie)
    for i in range(MAX):
        for j in range(MAX):
            vivant = 0
            if (tmp[((j + MAX - 1) % MAX, i % MAX)]):
                vivant += 1
            if (tmp[((j + MAX - 1) % MAX, (i + MAX - 1) % MAX)]):
                vivant += 1
            if (tmp[((j + MAX - 1) % MAX, (i + 1) % MAX)]):
                vivant += 1
            if (tmp[((j + 1) % MAX, i % MAX)]):
                vivant += 1
            if (tmp[((j + 1) % MAX, (i + MAX - 1) % MAX)]):
                vivant += 1
            if (tmp[(j + 1) % MAX, (i + 1) % MAX]):
                vivant += 1
            if (tmp[j % MAX, (i + MAX - 1) % MAX]):
                vivant += 1
            if (tmp[j % MAX, (i + 1) % MAX]):
                vivant += 1

            if (tmp[j, i]):
                if (vivant >= 2 and vivant <= 3):
                    vie[(j, i)] = 1
                if (vivant >= 4):
                    vie[(j, i)] = 0
                if (vivant <= 1):
                    vie[(j, i)] = 0
            else:
                if (vivant == 3):
                    vie[(j, i)] = 1
    return vie


#########################################################################
# nom : lancer_jeu
# valeurs entree : nombre de case du tableau.
# valeurs sortie : aucune
# fonction : initialiser le jeu et l'organiser.
#########################################################################

def dessiner():
    x = 0
    y = 0
    for i in range(size):
        x = 0
        for j in range(size):
            if grille[(i,j)]==1:
                can.create_rectangle(x, y, x + t_cellule, y + t_cellule, fill = c_cellule, outline = "black")
            else:
                can.create_rectangle(x, y, x + t_cellule, y + t_cellule, fill = c_case_vide, outline = "black")
            # on incrémente pour arriver sur la position du bloc suivante
            x = x + t_cellule
        # on incremente pour arriver sur la ligne suivante
        y = y + t_cellule

def animation():
    global drapeau_animation
    if drapeau_animation:
        global grille
        grille = effectuer(grille, taille)
        can.delete(ALL)
        #on dessine la nouvelle grille
        dessin()
        #pour générer la génération suivante : 
        fenetre.after(tmps_genp, animation)
    

def lancer_jeu(MAX=40):
    vie = {}
    init(vie, MAX)
    # while (1):
    while(drapeau == 1):
        afficher(vie, MAX)
        sleep(1)
        os.system("cls")  # in windows : cls instead of clear in linux
        vie = effectuer(vie, MAX)

def pressedLancer():
    global drapeau
    drapeau = 1
    val = taille.get()
    lancer_jeu(val)


def pressedArreter():
    global drapeau
    drapeau = 0
    print("j'ai appuyé sur arrêter haha")

def pressedInitialiser():
    print("je veux initialiser le jeu de la vie")
    global grille
    global drapeau_animation
    global drapeau_lancer
    global taille
    global can
    global vie
    drapeau_animation = 0
    drapeau_lancer = 0
    
    #on modifie la taille du canvas
    taille = int(scale3.get())
    can["height"]=taille * t_cellule
    can["width"]=taille * t_cellule
    
    #on modifie le % de vie à l'initialisation :
    pourcentage = int(scale2.get())
    vie = int(100/pourcentage)
    
    #on initialise la nouvelle grille 
    init_dam(grille, taille)

    #on dessine
    dessin()
        
    global tmps_gen
    tmps_gen = int(1000/int(scale.get()))
    

#########################################################################
# nom : main
# valeurs entree : aucune.
# valeurs sortie : aucune
# fonction : initialiser le jeu et l'organiser.
#########################################################################
#def main():
initialiser_dam_zero(grille, taille)

window = Tk()
window.title("Jeu de la vie de Côme et Julie")

quadrillage = Frame(window, bd=2)
quadrillage.pack(side=LEFT)
menu = Frame(window, bd=2)
menu.pack(side=RIGHT)

can = Canvas(quadrillage, height = taille * t_cellule, width = taille * t_cellule, bg = "white")
can.focus_set()
can.pack()

lancer = Button(menu, text="Lancer", command=pressedLancer)
lancer.pack()

arreter = Button(menu, text="Arrêter", command=pressedArreter)
arreter.pack()

initialiser = Button(menu, text="Initialiser", command=pressedInitialiser)
initialiser.pack()

taille_dam = IntVar()
choix_taille = Scale(menu, variable= taille_dam, orient=HORIZONTAL, from_=10, to=60, resolution=10, label="Taille")
choix_taille.pack()

global vitesse
vitesse = IntVar()
choix_vitesse = Scale(menu, variable = vitesse, orient=HORIZONTAL, from_=1, to=5, resolution=1, label="Vitesse")
choix_vitesse.pack()

message = Label(quadrillage, text = "A")
message.pack()
#lancer_jeu(20)
window.mainloop()


#if __name__ == "__main__":
#   main()
